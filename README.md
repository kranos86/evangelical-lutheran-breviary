![](https://gitlab.com/kranos86/evangelical-lutheran-breviary/-/raw/master/img/Logo11.png)

# Evangelical Lutheran Breviary

*Evangelical Lutheran Breviary* is a project to create and compile a complete Liturgy of the Hours in the Lutheran tradition, based on the work done in the publication of *Evangelical Lutheran Worship* (Augsburg Fortress, 2006) that is entirely in the public domain and in ebook format (primarily `.epub`). It includes liturgies for each hour, and work is ongoing to add a complete daily lectionary.

## Why?

The current hymnals and liturgies of the Lutheran churches in North America (*Evangelical Lutheran Worship*, 2006; *Lutheran Service Book*, 2006; *ReClaim Hymnal*, 2006; *Evangelical Lutheran Hymnary*, 1996; *Christian Worship*, 1993) are all under copyright and their use is regulated and restricted by their associated churches.

The liturgy and prayers of the church are the gifts of God to and for the people of God. It is my belief that they should be freely available to Christians without monetary cost and without restrictions. Charging money for the use of the liturgies of the church restricts their use to those who can afford to pay for the privilege. Restricting who can use them, and for what purposes, robs the Christian of the freedom to worship as they need.

## Using

`.epub` releases can be opened in any app or program that reads `.epub ` files.

### Building

- [Sigil](https://sigil-ebook.com/get/) (if using GNU/Linux, check your distribution's repositories)
- [FolderIn Plugin](https://www.mobileread.com/forums/showthread.php?t=293649)

1. Download the files in the `epub` folder.
2. Import them into Sigil using the FolderIn plugin.
3. Save as a `.epub` archive.

## License


<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
       href="http://creativecommons.org/publicdomain/zero/1.0/">
           <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" align=center alt="CC0" />
             </a>
               <br />
                 To the extent possible under law, except for the liturgical texts published by the English Language Liturgical Consultation,
                   <a rel="dct:publisher"
                        href="https://evangelicallutheranbreviary.wordpress.com">
                            <span property="dct:title">Ken Ranos</span></a>
                              has waived all copyright and related or neighboring rights to the
                                <span property="dct:title"><cite>Evangelical Lutheran Breviary</cite></span>.
                                This work is published from:
                                <span property="vcard:Country" datatype="dct:ISO3166"
                                      content="US" about="https://evangelicallutheranbreviary.wordpress.com">
                                        United States</span>.
                                        </p>

<b>except</b>

English translation of "Apostles' Creed" © 1988 English Language Liturgical Consultation (ELLC). http://www.englishtexts.org. Used by permission. ELLC texts may be used and adapted, provided this attribution is included.

While the texts are not in the public domain, at present they remain the most recent and faithful translations of the texts. Their sole use requirement, simple attribution, is considered to be a reasonable expectation, and does not warrant their exclusion in this otherwise CC0 work.
